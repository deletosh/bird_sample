### Setup
This assumes you have [docker](https://docs.docker.com/v17.09/engine/installation/) installed and running.
- `git clone git@gitlab.com:iamdeletosh/bird_sample.git`
- `cd docker-firmbird`
- `cp env-example .env` (this is based on laradock. See [this "Usage" section](https://laradock.io/getting-started/) on configuring this file)
- `docker-compose build workspace nginx mariadb`
- `docker-compose up -d  workspace nginx mariadb`
- http://localhost/admin
	- username: admin@authwit.dev
	- pwd: 1234


### Links:
Administrators/staff: http:/localhost/admin

	- username: admin@authwit.dev
	- pwd: 1234
	
Customers: http://localhost/clients


### Mainline Guide (based on Perfex CRM):
- https://help.perfexcrm.com/

### Troubleshooting
If you have trouble getting the app with it complainaing of "imap" -- run this command after entering the container.

 apt-get update && \
    apt-get install -y libc-client-dev libkrb5-dev && \
    rm -r /var/lib/apt/lists/* && \
    docker-php-ext-configure imap --with-kerberos --with-imap-ssl && \
    docker-php-ext-install imap \

>    mariabdb