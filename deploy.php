<?php
namespace Deployer;

require 'recipe/codeigniter.php';

// Project name
set('application', 'firmbird');
set('ssh_multiplexing', false);

set('keep_releases', 3);

// Project repository
set('repository', 'git@gitlab.com:authoritywit/apps/firmbird.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);
inventory('hosts.yml');

// Shared files/dirs between deploys 
add('shared_files', []);
add('shared_dirs', []);

// Writable dirs by web server 
add('writable_dirs', []);
set('allow_anonymous_stats', false);
    
// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

