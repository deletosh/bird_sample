<?php

defined('BASEPATH') or exit('No direct script access allowed');

class paystack_gateway extends App_gateway
{

    public function __construct()
    {
        $this->ci = &get_instance();

        /**
         * Call App_gateway __construct function
         */
        //parent::__construct();
        /**
         * REQUIRED
         * Gateway unique id
         * The ID must be alpha/alphanumeric
         */
        $this->setId('paystack');

        /**
         * REQUIRED
         * Gateway name
         */
        $this->setName('paystack');

        /**
         * Add gateway settings
         */
        $this->setSettings(
            [
                [
                    'name' => 'paystack_public_key',
                    'encrypted' => true,
                    'label' => 'Public Key',
                ],
                [
                    'name' => 'paystack_Secret_key',
                    'encrypted' => true,
                    'label' => 'Secret key',
                ],
                [
                    'name' => 'paystack_test_Secret_key',
                    'encrypted' => true,
                    'label' => 'test Secret key',
                ],
                [
                    'name' => 'currencies',
                    'label' => 'settings_paymentmethod_currencies',
                    'default_value' => 'NGN',
                    'field_attributes' => ['disabled' => true],
                ],
                [
                    'name' => 'test_mode_enabled',
                    'type' => 'yes_no',
                    'default_value' => 1,
                    'label' => 'settings_paymentmethod_testing_mode',
                ],
            ]
        );
    }

    /**
     * REQUIRED FUNCTION
     * @param array $data
     * @return mixed
     */
    public function process_payment($data)
    {

        if ($this->getSetting('test_mode_enabled') == '1') {
            $paystacksecret = $this->decryptSetting('paystack_test_Secret_key');

        } else {
            $paystacksecret = $this->decryptSetting('paystack_Secret_key');
        }

        $paystack = new Yabacon\Paystack($paystacksecret);
        $reference = format_invoice_number($data['invoice']->id) . '-' . time();


        $calurl = site_url('paystack/verify?invoiceid=' . $data['invoiceid'] . '&hash=' . $data['invoice']->hash);
        $pamount = number_format($data['amount'], 2, '.', '');


        try {
            $buyer_name = null;
            $email = null;
            $phonenumber = null;
            $pamount = number_format($data['amount'], 2, '.', '');
            $koboamount = $pamount * 100;


            if (is_client_logged_in()) {
                $contact = $this->ci->clients_model->get_contact(get_contact_user_id());
                $buyer_name = $contact->firstname . ' ' . $contact->lastname;
                if ($contact->email) {
                    $email = $contact->email;
                }
                if ($contact->phonenumber) {
                    $phonenumber = $contact->phonenumber;
                }
            } else {
                $contacts = $this->ci->clients_model->get_contacts($data['invoice']->clientid);
                if (count($contacts) == 1) {
                    $contact = $contacts[0];
                    $buyer_name = $contact['firstname'] . ' ' . $contact['lastname'];
                    if ($contact['email']) {
                        $email = $contact['email'];
                    }
                    if ($contact['phonenumber']) {
                        $phonenumber = $contact['phonenumber'];
                    }
                }
            }

            $tranx = $paystack->transaction->initialize([
                'amount' => $koboamount,       // in kobo
                'email' => isset($email) ? $email : 'no-reply@user.com',
                'reference' => $reference,
                'callback_url' => $calurl,

            ]);


        } catch (Exception $e) {
            //  $errors = $e->getResponseObject();
            //echo $e;
            set_alert(' payment not sent <br/> please check your secret key');
        } catch (\Yabacon\Paystack\Exception\ApiException $e) {
            $errors = $e->getResponseObject();
            // echo $e;
            set_alert('danger', _l($errors->message . ' payment not sent <br/> please check your secret key'));
        }
        header('Location: ' . $tranx->data->authorization_url);

    }


}
