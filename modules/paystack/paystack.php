<?php
/**
 * Ensures that the module init file can't be accessed directly, only within the application.
 */
defined('BASEPATH') or exit('No direct script access allowed');
/*
Module Name: Paystack
Description: Paystack module for invoice payment.
Version: 1.0.0
Requires at least: 2.3.*
*/
require(__DIR__ . '/vendor/autoload.php');
/**
 * Module URL
 * e.q. https://crm-installation.com/module_name/
 * @param  string $module  module system name
 * @param  string $segment additional string to append to the URL
 * @return string
 */
// module_libs_path('paystack/libraries', $concat = '');
//module_dir_url('paystack', $segment = '');
register_payment_gateway('paystack_gateway', 'paystack');
